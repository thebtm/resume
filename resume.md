# Brad Mouck   
47 McKinnon St, Langdon AB - [brad.mouck@thebtm.ca](mailto:brad.mouck@thebtm.ca) - [LinkedIn Profile](https://www.linkedin.com/in/bradmouck/)   


### Professional Summary
Driven self-taught IT professional with 7 years of experience in Backup and Linux System Administration included troubleshooting, maintaining, and improving systems and procedures. Also 2 years of PHP full-stack development, fixing bugs, adding features and version upgrades.

### Employment
###### Storage & Backup Analyst  
June 2015 - Dec 2018  
Shaw Communications, Calgary, Alberta

Added to my Backup Analyst responsibilities:  
* Administration, maintenance, and On-Call Support of NetApp storage systems.  
* Configured storage for as LUNs or NAS storage for vmware environments, Oracle Databases, and other storage related tasks.  
* Create bash scripts for management of backup and storage processes.  
  * Automatic tape ordering with Iron Mountain's tape management system to remove daily task from team.  
  * Improved NetApp monitoring alerts for better responce time.  

###### Full Stack PHP Developer
Apr 2016 - Dec 2018  
Shaw Communications, Calgary, Alberta

* Upgraded internal built CMDB PHP tool from PHP 4 to PHP 5 and to PHP 7.
* Improved lag time issues with internal tool within the MySQL Database by enabling features.
* Added JavaScript and jQuery to the site for better handling of site loading.
* Bug fix and expanded tool for better reporting and functionality.
* Built an integration API using JSON to communicate for inhouse tool to communicate with Service Now reducing level of efforts of team members.
* Implimented git version control system for faster deployment into production


###### Backup Analyst
Dec 2011 - June 2015  
Shaw Communications, Calgary, Alberta

* Administration and maintenance Veritas NetBackup
  * tape handling, restores, troubleshooting, policy configurations, vaulting, installing, upgrading, replacing and setting up NetBackup servers and infrastructure.
* Working and monitoring HP ESL & MSL Tape libraries on a GUI and Command Line interfaces.


###### Information Technology - Service Center Analyst
Sept 2008 - Dec 2011  
Shaw Communications, Calgary, Alberta

* Review and action IT requests for the various IT teams and completed some of the work on behalf of other IT teams.
* Basic desktop troubleshooting & software installs.
* Active Directory administration for creating and managing mailboxes, unlocks, password resets, account disables, information updates and access permissions on Primary and Secondary domains.
* Look at business gaps and improve the process if there is a way to improve. Support user access for various Internal Tools.
* Monitor and support servers’ alerts on a 24 hour basis.
* Front line role for the IT Team at Shaw. Responsible for taking IT Requests and inquiry by phone.


### Skills
###### Expert
* Veritas NetBackup
* Troubleshooting & Problem Solving

###### Proficient
* RHEL family of distributions
* PowerShell, Bash, Git, SED
* PHP, HTML, JavaScript, jQuery, AJAX
* MySQL, MariaDB

###### Familiar
* NetApp CDoT 8.x, 9.x
* CommVault, Veeam Backup Solutions
* AWS, Docker
* Ubuntu, Debian, Solaris, HPUX, AWK
* .NET, C#, VB, VBA, Active Directory
* CSS
* HP Service Desk, Remedy, and Service Now

###### Soft Skills
* Customer Service
* Very friendly & positive attitude
* Strong Integrity
* Exceptional teamwork

###### Learning
* GameMaker Studio 2


### Education
#### 2D Game Design Certificate
SAIT - Jan 2019 - Mar 2019  

#### Computer Systems Technology
SIAST Kelsey - Sep 2004 - Jun 2005

#### Electronics Engineering Technology
SIAST Palliser - Sep 2003 - Jun 2004


### Certificates of Completion
###### Vendor Specialized Classes
* Clustered Data OnTab Administration 8.3
* HP StoreFabric B-Series Switch Administration
* HP StoreFabric B-Series Switch Professional
* RedHat System Administration I

###### Lynda.com
* SED Training
* AWK Essential Training
* Linux: Multitasking at the Command Line
* Git for teams
* Git Intermediate Techniques
* GitHub for Web Designers
* Git Essential Training
* PHP: Test-Driven Development with PHP unit
* Learning Docker (2016)
* Amazon Web Services Essential Training
* Learning Atom (2016)
* Ethical Hacking: Overview
